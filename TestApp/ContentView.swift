//
//  ContentView.swift
//  TestApp
//
//  Created by Jan Kicina on 24/05/2021.
//

import SwiftUI
import MyTestSwiftPackage

let test = MyTestSwiftPackage()

struct ContentView: View {
    var body: some View {
        Text(test.getText())
            .padding()
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
