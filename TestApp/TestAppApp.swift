//
//  TestAppApp.swift
//  TestApp
//
//  Created by Jan Kicina on 24/05/2021.
//

import SwiftUI
import MyTestSwiftPackage

@main
struct TestAppApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
